runtime! plugin/**/*.vim
" {{{ Plugins
call plug#begin('~/.local/share/nvim/plugged')

" editing
Plug 'tpope/vim-surround'           " Add, change, remove parentheses
Plug 'tpope/vim-commentary'         " (Un)comment blocks/lines easily
Plug 'tpope/vim-unimpaired'         " paired keybinding using [ ] 
Plug 'tpope/vim-repeat'             " '.' for plugins
Plug 'tpope/vim-rsi'                " readline keybindings
Plug 'tpope/vim-dispatch'           " run things in tmux from within vim
Plug 'tpope/vim-obsession'          " better session handling

" working with files
Plug 'tpope/vim-fugitive'           " Git Wrapper
Plug 'tpope/vim-rhubarb'            " GitHub extension for fugitive
Plug 'junegunn/gv.vim'              " Git Commit browser
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " fuzzy finder
Plug 'junegunn/fzf.vim'             " Wrapper around fzf

" filetypes 
Plug 'neomutt/neomutt.vim'          " sets filetypes and has syntax for neomuttrc 
Plug 'cespare/vim-toml'             " add syntax support for .toml files

" Ruby (on Rails)
Plug 'tpope/vim-rails'
Plug 'tpope/vim-bundler'

" themes
Plug 'morhetz/gruvbox'
Plug 'cormacrelf/vim-colors-github'

" deoplete
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

" LanguageClient
" Plug 'autozimu/LanguageClient-neovim', {
"             \ 'branch': 'next',
"             \ 'do': 'bash install.sh' }

" unified tmux vim movement
Plug 'christoomey/vim-tmux-navigator'
call plug#end()

" Completion settings
" Keybindings for LanguageClient are in ftplugins
" let g:deoplete#enable_at_startup = 1
" let g:LanguageClient_serverCommands = {
"             \ 'python': ['pyls'],
"             \ 'cpp': ['clangd'],
"             \ }
let g:tmux_navigator_no_mappings = 1

" Preview for Rg: toggle with '?'
command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)
" }}}

" {{{ General
filetyp plugin indent on " redundant (vim-plug sets this already)
set hidden               " change buffers without saving
set autoindent
set smartindent

" whitespace
set linebreak       " don't break words (see :h breakat)
set textwidth=0     " no automatic newlines
set tabstop=8       " size of tab
set shiftwidth=4    " indent by autoindent
set expandtab       " insert spaces in place of tabs
set softtabstop=4   " amount of spaces to replace a tab
set splitright

" line numbers
set number
set scrolloff=3
set sidescrolloff=3

set inccommand=nosplit " Live-Preview of e.g. s/foo/bar
"}}}

" {{{ theme
set termguicolors                       " 'true colors'
" gruvbox settings
let g:gruvbox_contrast_light="hard"
let g:gruvbox_contrast_dark="medium"
let g:gruvbox_italic = 0
let g:gruvbox_bold = 0
colorscheme github

" setup visual mark for long lines
" disable colorcolumn for all lines
set colorcolumn=
" apply colorcolumn to lines going over 80
call matchadd('ColorColumn', '\%80v')
" }}}

" {{{ statusline
" always show statusline
set laststatus=2
" first thing to show: current column (2 digits)
set statusline=
set statusline+=%02.c\  
" cut here if needed
set statusline+=%<
" filename
set statusline+=%-30f\ 
" flags
set statusline+=%m%r%w\ 
" Git info
set statusline+=%{fugitive#statusline()}\ 
" end of left side
set statusline+=%=
" quickfix/location list if not empty
set statusline+=%q
" filetype
set statusline+=%y\ 
" filesize
set statusline+=%{FileSize()}\ 
" current line / total lines
set statusline+=%l/%L
" }}}

"{{{ netrw (File Explorer)
let g:netrw_banner = 0
let g:netrw_liststyle = 3 " use 'i' to cycle styles

" 0 current window (default)
" 1 horizontal split
" 2 vertical split
" 3 new tab
" 4 prev window
" let g:netrw_browse_split = 2

" size in % of width
let g:netrw_winsize = 25
" }}}

" {{{ MAPPINGS
" Space is leader
let mapleader = " "
let g:mapleader = " "
" move between tmux and vim
nnoremap <silent> <A-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <A-j> :TmuxNavigateDown<cr>
nnoremap <silent> <A-k> :TmuxNavigateUp<cr>
nnoremap <silent> <A-l> :TmuxNavigateRight<cr>
nnoremap <silent> <C-\> :TmuxNavigatePrevious<cr>
tnoremap <silent> <A-h> <C-\><C-N>:TmuxNavigateLeft<cr>
tnoremap <silent> <A-j> <C-\><C-N>:TmuxNavigateDown<cr>
tnoremap <silent> <A-k> <C-\><C-N>:TmuxNavigateUp<cr>
tnoremap <silent> <A-l> <C-\><C-N>:TmuxNavigateRight<cr>
" Move around in insert mode
inoremap <C-h> <left>
inoremap <C-l> <right>
inoremap <C-k> <up>
inoremap <C-j> <down>
" make Y behave like C and D (yank to end of line)
nnoremap Y y$
" move in wrapped lines
nnoremap j gj
nnoremap k gk
" quickly edit $vimrc
nnoremap <leader>ve :vsplit $MYVIMRC<cr>
nnoremap <leader>vs :source $MYVIMRC<cr>
" open vimrc ftplugins
nnoremap <leader>vp :vsplit <C-r>=expand("~") . "/.config/nvim/after/ftplugin"<CR><CR>
" save current file
nnoremap <leader>w :w!<cr>
" ... with sudo
nnoremap <leader>W :w !sudo tee % > /dev/null<cr>
" quick replace (word under cursor)
nnoremap <Space><Space> :'{,'}s/\<<C-r>=expand('<cword>')<CR>\>/
nnoremap <Space>%       :%s/\<<C-r>=expand('<cword>')<CR>\>/
" yank and paste from clipboard
nnoremap <leader>y "+y
nnoremap <leader>p "+p
" disable search highlight on <leader><enter>
map <silent> <leader><cr> :noh<cr>
" FZF
nnoremap <leader>f :Rg 
nnoremap <leader>l :Lines 
nnoremap <leader>e :FZF <CR>
nnoremap <leader>gf :GFiles <CR>
nnoremap <leader>gc :Commits <CR>
nnoremap <leader>gs :GFiles? <CR>
nnoremap <leader>b :Buffers <CR>
" Delete trailing whitespace
nnoremap <leader>x <Plug>DeleteTrailingWhitespace
" }}}

" {{{ FUNCTIONS

" create html from markdown using pandoc
function! MDToHTML()
    silent execute "!pandoc -s -f markdown_github -t html -o " . expand("%:r") . ".html " . expand("%")
endfunction
command! MDToHTML call MDToHTML()

" create filesize string
function! FileSize()
    let bytes = getfsize(expand("%:p"))
    if bytes <= 0
        return ""
    endif
    if bytes < 1024
        return bytes . "B"
    elseif bytes < 1048576
        return (bytes / 1024) . "KB"
    else
        return (bytes / 1048576) . "MB"
    endif
endfunction

" make list-like commands more intuitive
function! CCR()
    let cmdline = getcmdline()
    if cmdline =~ '\v\C^(ls|files|buffers)'
        " like :ls but prompts for a buffer command
        return "\<CR>:b"
    elseif cmdline =~ '\v\C/(#|nu|num|numb|numbe|number)$'
        " like :g//# but prompts for a command
        return "\<CR>:"
    elseif cmdline =~ '\v\C^(dli|il)'
        " like :dlist or :ilist but prompts for a count for :djump or :ijump
        return "\<CR>:" . cmdline[0] . "j  " . split(cmdline, " ")[1] . "\<S-Left>\<Left>"
    elseif cmdline =~ '\v\C^(cli|lli)'
        " like :clist or :llist but prompts for an error/location number
        return "\<CR>:sil " . repeat(cmdline[0], 2) . "\<Space>"
    elseif cmdline =~ '\C^old'
        " like :oldfiles but prompts for an old file to edit
        set nomore
        return "\<CR>:sil se more|e #<"
    elseif cmdline =~ '\C^changes'
        " like :changes but prompts for a change to jump to
        set nomore
        return "\<CR>:sil se more|norm! g;\<S-Left>"
    elseif cmdline =~ '\C^ju'
        " like :jumps but prompts for a position to jump to
        set nomore
        return "\<CR>:sil se more|norm! \<C-o>\<S-Left>"
    elseif cmdline =~ '\C^marks'
        " like :marks but prompts for a mark to jump to
        return "\<CR>:norm! `"
    elseif cmdline =~ '\C^undol'
        " like :undolist but prompts for a change to undo
        return "\<CR>:u "
    else
        return "\<CR>"
    endif
endfunction
" call function when pressing <CR> in command mode
cnoremap <expr> <CR> CCR()
" }}}

" {{{ Persistent editing
" reload files when saved externally
set autoread
" Use persistent history.
if has('persistent_undo')
    if !isdirectory("/tmp/.vim-undo-dir")
        call mkdir("/tmp/.vim-undo-dir", "", 0700)
    endif

    set undodir=/tmp/.vim-undo-dir
    set undofile
endif
" }}}

" {{{ autocmd groups
" TODO: investigate what plugin is manipulating formatoptions
" ^^^ looks like default ftplugins set formatoptions with to 'jcroql'
" this hack is just a workaround to disable automatic comment
" format on 'o' and 'O'
" augroup fix_formatoptions
"     autocmd!
"     autocmd FileType * :setlocal formatoptions-=o
" augroup end

augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup end

augroup filetype_qml
    autocmd!
    autocmd BufNewFile,BufRead *.qml setfiletype qml
augroup end

augroup window_resize
    autocmd!
    autocmd VimResized * :wincmd =
augroup end

" }}}

