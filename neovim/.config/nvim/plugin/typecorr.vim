" Vim global plugin for correcting typing mistakes
" Tutorial from :h write-plugin

" load guard
if exists("g:loaded_typecorr")
    finish
endif
" vim-global variable
let g:loaded_typecorr = 1

" script local variable
let s:save_cpo = &cpo
set cpo&vim


iabbrev teh the
iabbrev otehr other
iabbrev wnat want
iabbrev synchronisation synchronization
let s:count = 4

if !hasmapto('<Plug>TypecorrAdd')
    map <unique> <Leader>a <Plug>TypecorrAdd
endif
noremap <unique> <script> <Plug>TypecorrAdd <SID>Add
noremap <SID>Add :call <SID>Add(expand("<cword>"), 1)<CR>

function s:Add(from, correct)
let to = input("type the correct for " . a:from . ": ")
exe ":iabbrev " . a:from . " " . to
endfunction

let &cpo = s:save_cpo
