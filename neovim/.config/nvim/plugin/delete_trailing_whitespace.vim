if exists('g:loaded_DeleteTrailingWhitespace')
    finish
endif
let g:loaded_DeleteTrailingWhitespace = 1

function s:DeleteTrailingWhitespace()
    if !&binary && !&filetype != 'diff'
        let s:saved_winview = winsaveview()
        keeppatterns %s/\v\s+$//e
        call winrestview(s:saved_winview)
    endif
endfunction

nnoremap <unique> <script> <Plug>DeleteTrailingWhitespace DeleteTW
nnoremap <SID>DeleteTW :<C-U>call <SID>DeleteTrailingWhitespace()<CR>
command PruneWS call <SID>DeleteTrailingWhitespace()
