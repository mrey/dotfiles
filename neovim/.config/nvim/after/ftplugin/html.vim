set softtabstop=2
set shiftwidth=2
inoremap <buffer> <c-k> </<c-x><c-o>
inoremap <buffer> <M-t> <title></title><Esc>7hi
inoremap <buffer> <M-b> <body><cr></body><Esc>O
inoremap <buffer> <M-2> <head><cr></head><Esc>O
inoremap <buffer> <M-1> <html><cr></html><Esc>O
