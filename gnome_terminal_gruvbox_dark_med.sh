#!/bin/sh

PROFILE_ID=$1

echo "setting bg"
dconf write /org/gnome/terminal/legacy/profiles:/$PROFILE_ID/background-color "'#282828'"
echo "setting fg"
dconf write /org/gnome/terminal/legacy/profiles:/$PROFILE_ID/foreground-color "'#ebdbb2'"

echo "setting palette"
dconf write /org/gnome/terminal/legacy/profiles:/$PROFILE_ID/palette "['#282828', '#cc241d', '#98971a', '#d79921',
'#458588', '#b16286', '#689d6a', '#a89984', '#928374', '#fb4934', '#b8bb26', '#fabd2f', '#83a598', '#d3869b',
'#8ec07c', '#ebdbb2']"
