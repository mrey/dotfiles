set -g history-limit 50000
# set ZSH as default shell
set-option -g default-shell /usr/bin/zsh

# true colors and italics
# set -g default-terminal "tmux"
set -g default-terminal "xterm-256color"
set-option -ga terminal-overrides ",xterm-256color:Tc"

# escape time for nvim
set-option -sg escape-time 0
set-option -g allow-rename off

unbind C-b
set -g prefix C-Space
bind C-Space send-prefix

# source config via prefix R
bind R source-file ~/.tmux.conf \; display-message "Config reloaded..."

# use PREFIX | to split window horizontally and PREFIX - to split vertically
bind | split-window -h -c '#{pane_current_path}'
bind - split-window -v -c '#{pane_current_path}'

# back to last window
bind p last-window

# map Vi movement keys as pane movement keys
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
# map ALT + Vi movement without prefix
# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator
passthrough_wanted="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(weechat|view|n?vim?x?)(diff)?$'"
bind-key -n M-h if-shell "$passthrough_wanted" "send-keys M-h"  "select-pane -L"
bind-key -n M-j if-shell "$passthrough_wanted" "send-keys M-j"  "select-pane -D"
bind-key -n M-k if-shell "$passthrough_wanted" "send-keys M-k"  "select-pane -U"
bind-key -n M-l if-shell "$passthrough_wanted" "send-keys M-l"  "select-pane -R"
bind-key -n C-\ if-shell "$passthrough_wanted" "send-keys C-\\" "select-pane -l"
bind-key -T copy-mode-vi M-h select-pane -L
bind-key -T copy-mode-vi M-j select-pane -D
bind-key -T copy-mode-vi M-k select-pane -U
bind-key -T copy-mode-vi M-l select-pane -R
bind-key -T copy-mode-vi C-\ select-pane -l

# vi-style in copy mode
set-option -g status-keys vi
set-window-option -g mode-keys vi

# and use C-h and C-l to cycle thru panes
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+
bind -nr M-C-h select-window -t :-
bind -nr M-C-l select-window -t :+

# resize panes using C-[hjkl]
bind -r H resize-pane -L
bind -r J resize-pane -D
bind -r K resize-pane -U
bind -r L resize-pane -R

# separator
set -g pane-active-border-style "fg=blue bg=default"
set -g pane-border-style "fg=white"

# statusbar
set-window-option -g status-left " [ #S ] "
set-window-option -g status-right " [ %H:%M @ #H ] "

# active
set-window-option -g window-status-current-format " #I:#W(#F) "
set-window-option -g window-status-current-style "fg=blue bg=black"

# inactive
set-window-option -g status-style "fg=white bg=black"
set-window-option -g window-status-format " #I:#W#{?window_flags,(#F),""} "
