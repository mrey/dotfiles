source "gpg -dq /home/alex/.config/neomutt/mail_pw.gpg |"
set mailcap_path="/home/alex/.config/neomutt/mailcap"
set realname='Alexander Graul'
set from="agraul@suse.com"
set use_from=yes
set folder="~/Maildir/"
set spoolfile="~/Maildir/INBOX"
set header_cache="~/Localmail/hcache"
set record="~/Maildir/INBOX.Sent"
set postponed="~/Maildir/INBOX.Drafts"
set mbox_type=Maildir
set smtp_url="smtp://agraul@suse.de@imap.suse.de:587"
set ssl_starttls=yes
mailboxes =INBOX =INBOX.Sent =INBOX.Drafts =INBOX.trello =INBOX.DeepSea \
          =INBOX.mailinglists-suse               =INBOX.mailinglists-suse.kiga \
          =INBOX.mailinglists-suse.devel         =INBOX.mailinglists-suse.users \
          =INBOX.mailinglists-suse.research      =INBOX.mailinglists-suse.maxtorhof \
          =INBOX.mailinglists-suse.vpn           =INBOX.mailinglists-suse.talk \
          =INBOX.mailinglists-suse.deepsea-users =INBOX.mailinglists-suse.results \
          =INBOX.mailinglists-suse.qa-sle        =INBOX.mailinglists-suse.qa-sle-nbg \
          =INBOX.mailinglists-suse.qa-team       =INBOX.mailinglists-suse.qa-reports \
          =INBOX.Spam =INBOX.Trash
          # =INBOX.mailinglists-suse.bs-team       =INBOX.mailinglists-suse.ceph \
 
# Groups
alternates -group suse agraul@suse.com agraul@suse.de
alternates -group personal agraul@opensuse.org mail@agraul.de mail@alexandergraul.de alexander.graul@mailbox.org merk@posteo.de

# Mailing lists
subscribe kiga
subscribe devel
subscribe users
subscribe research
subscribe vpn
subscribe maxtorhof
subscribe talk
subscribe ceph
subscribe ceph-bugs
subscribe deepsea-users
subscribe ses-users
subscribe qa-sle
subscribe qa-sle-ngb
subscribe qa-reports
subscribe qa-team
subscribe results

lists bs-team@suse.de

# Threaded list views, autocollapsed
folder-hook . "set sort=date-sent"
folder-hook mailinglists-suse "set sort=threads"
folder-hook mailinglists-suse 'push <collapse-all>'

auto_view text/html
alternative_order text/plain text/enriched text/html
set skip_quoted_offset = 3
set pager_context = 3

set sidebar_visible
set sidebar_format="%B%?F? [%F]?%* %?N?%N/?%S"
set sidebar_width = 35
set sidebar_short_path
set sidebar_delim_chars="/."
set sidebar_folder_indent   # indent shortened folder names
set sidebar_indent_string="  "
set sidebar_divider_char = '│'
set mail_check_stats

# Vim like binds
bind attach,index,pager \CD next-page
bind attach,index,pager \CU previous-page
bind index,pager gg noop 
bind index,pager g noop   # unbind g for the gg binding
bind index,pager gg top
bind index,pager G bottom
bind index,pager J next-unread
bind index,pager K previous-unread
bind attach,index g first-entry
bind attach,index G last-entry

# Sidebar binds
bind index,pager \CP sidebar-prev                 # Ctrl-n to select next folder
bind index,pager \CN sidebar-next                 # Ctrl-p to select previous folder
bind index,pager \CO sidebar-open                 # Ctrl-o to open selected folder
bind index,pager \CB sidebar-toggle-visible       # Ctrl-b to toggle visibility of the sidebar

# Other binds
bind index X vfolder-from-query
bind index <Esc>U toggle-read

source  /home/alex/.config/neomutt/colors
source  /home/alex/.config/neomutt/gpgrc
source  /home/alex/.config/neomutt/aliases
set alias_file = /home/alex/.config/neomutt/aliases

set text_flowed=yes
