* Package Management setup

~use-package.el~ takes care of installing all packages. Package repos are org and melpa.
#+BEGIN_SRC emacs-lisp
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(setq package-enable-at-startup nil)
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(require 'use-package-ensure)
#+END_SRC
Packages that are not installed at startup will be installed automatically.
#+BEGIN_SRC emacs-lisp
(setq use-package-always-ensure t)
#+END_SRC

* Keybinding abstraction layer

~general.el~ has the benifit of a single keybinding declaration API,
no matter if ~evil~ or emacs keybindings are set up.
#+BEGIN_SRC emacs-lisp
(use-package general)
#+END_SRC

* Vim emulation
** Evil setup

~evil.el~ turns emacs into an integrated environment for Vim.
The ~:init~ section has three variables set:
- ~evil-want-C-u scroll t~: Scroll half-pages like in Vim
- ~evil-want-integration t~: This is a default setting, but it is required for ~evil-collection.el~. So for documentation I keep it here (for the time being).
- ~evil-want-keybinding nil~: This setting is required for ~evil-collection.el~
The ~:config~ section has a hook to make =_= part of a word, do ~w~
does not stop in the middle of =_=- connected words. The variable ~evil-move-cursor-back nil~ does what it says.
#+BEGIN_SRC emacs-lisp
(use-package evil
  :init
  (setq evil-want-C-u-scroll t)
  ;; the following two have to be like that for evil-collection (the
  ;; first is a default, but for documentation I put it here)
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  ;; settings after loading
  :config
  (add-hook 'prog-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))
  (setq evil-move-cursor-back nil)
  (evil-mode 1))
#+END_SRC

** Evil extensions
*** vim-surround for evil

Source: https://github.com/emacs-evil/evil-surround
#+BEGIN_SRC emacs-lisp
(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode 1))
#+END_SRC

*** vim-commentary for evil

Source: https://github.com/linktohack/evil-commentary
#+BEGIN_SRC emacs-lisp
(use-package evil-commentary
  :after evil
  :config
  (evil-commentary-mode 1))
#+END_SRC

*** matchit.vim (% to jump)

Source: https://github.com/redguardtoo/evil-matchit
#+BEGIN_SRC emacs-lisp
(use-package evil-matchit
  :after evil
  :config
  (global-evil-matchit-mode 1))
#+END_SRC
*** TODO vim-like in-/decreasing of numbers
Source: https://github.com/cofi/evil-numbers
The package can be installed with ~(use-package evil-numbers)~, but it requires setup (keybindings).
*** TODO Vim like tabs

Source: https://github.com/krisajenkins/evil-tabs
*** Evil keybindings for other modes

Source: https://github.com/emacs-evil/evil-collection
#+BEGIN_SRC emacs-lisp
(use-package evil-collection
  :after evil
:config
(evil-collection-init))
#+END_SRC
** Keybindings for Evil

Move over visual lines, rather than real lines.
#+BEGIN_SRC emacs-lisp
(general-def 'normal
 "j" 'evil-next-visual-line
 "k" 'evil-previous-visual-line)
#+END_SRC

Window movements without letting go of =ctrl=
#+begin_src emacs-lisp
(general-def 'normal
  :prefix "C-w"
  "C-h" 'evil-window-left
  "C-l" 'evil-window-right
  "C-j" 'evil-window-down
  "C-k" 'evil-window-up)
#+end_src

* Emacs completion framework

Source: https://github.com/abo-abo/swiper

Ivy is the completion framework itself, but it comes with two additional packages.
There is counsel, which is a collection of ivy-enhanced emacs commands
and swiper, which replaces isearch.

Both swiper and ivy are dependencies of counsel and are therefore pulled in automatically.
Opening a buffer in another frame or window are added. They can be
used by pressing =M-o=, then =f= or =j= on a selection.
#+BEGIN_SRC emacs-lisp
(use-package counsel
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (ivy-set-actions
    'ivy-switch-buffer
      '(("f" switch-to-buffer-other-frame "Other frame")
        ("j" switch-to-buffer-other-window "Other window"))))
#+END_SRC

** Keybindings for swiper, counsel & ivy
Replace isearch and minibuffer with counsel.

#+BEGIN_SRC emacs-lisp
(general-def
  "C-s" 'counsel-grep-or-swiper
  "M-x" 'counsel-M-x)
#+END_SRC

Use SPC as prefix for common commands.

#+BEGIN_SRC emacs-lisp
(general-def '(normal motion) 'override
  :prefix "SPC"
  "e" 'counsel-find-file
  "f" 'counsel-rg
  "i" 'ivy-resume
  "x" 'counsel-M-x)
#+END_SRC

* General Keybindings

Use SPC as prefix for common commands.
#+BEGIN_SRC emacs-lisp
(general-def '(normal motion) 'override
  :prefix "SPC"
  "b" 'switch-to-buffer
  "k" 'kill-buffer
  "u" 'universal-argument
  "w" 'save-buffer)
#+END_SRC
* Org-mode

Org-mode is already installed, otherwise the configuration in this file could not be applied.
As shown in this document, org-mode is powerful and great for organizing things in plain text.

** Configuration
   
The files to be used for agenda display.
#+BEGIN_SRC emacs-lisp
(setq org-agenda-files '("~/org/inbox.org"
                         "~/org/gtd.org"
                         "~/org/tickler.org"))
#+END_SRC
Targets for refiling entries with ‘C-c C-w’.
#+BEGIN_SRC emacs-lisp
(setq org-refile-targets '(("~/org/gtd.org"     :maxlevel . 3)
			    ("~/org/someday.org" :level    . 1)
			    ("~/org/tickler.org" :maxlevel . 2)))
#+END_SRC
Captures for org capture (see keybinding below). Org capture can
quickly file a TODO from anywhere inside emacs that land in the inbox
file.
#+BEGIN_SRC emacs-lisp
(setq org-capture-templates '(("t" "Todo [Inbox]" entry
				(file+headline "~/org/inbox.org" "Tasks")
				"* TODO %i%?")
			    ("a" "Todo [Inbox] with address" entry
				(file+headline "~/org/inbox.org" "Tasks")
				"* TODO %i%? \n %a")
			    ("T" "Tickler" entry
				(file+headline "~/org/tickler.org" "Tickler")
				"* %i%? \n %U")))
#+END_SRC
Custom filters for org agenda, useful for GTD.
#+BEGIN_SRC emacs-lisp
(setq org-agenda-custom-commands 
    '(("o" "NEXT @office" tags-todo "@office"
	((org-agenda-overriding-header "Office")
	(org-agenda-skip-function #'my-org-agenda-skip-non-next)))
    ("c" "NEXT @computer" tags-todo "@computer"
	((org-agenda-overriding-header "Computer")
	(org-agenda-skip-function #'my-org-agenda-skip-non-next)))
    ("p" "NEXT @phone" tags-todo "@phone"
	((org-agenda-overriding-header "Phone")
	(org-agenda-skip-function #'my-org-agenda-skip-non-next)))
    ("h" "NEXT @home" tags-todo "@home"
	((org-agenda-overriding-header "Home")
	(org-agenda-skip-function #'my-org-agenda-skip-non-next)))
    ("e" "NEXT @email" tags-todo "@email"
	((org-agenda-overriding-header "Email")
	(org-agenda-skip-function #'my-org-agenda-skip-non-next)))))

(defun my-org-agenda-skip-non-next ()
  "Skip all but the first non-next entry."
  (let (should-skip-entry)
    (unless (org-current-is-next)
      (setq should-skip-entry t))
    (save-excursion
      (while (and (not should-skip-entry) (org-goto-sibling t))
        (when (org-current-is-next)
          (setq should-skip-entry t))))
    (when should-skip-entry
      (or (outline-next-heading)
          (goto-char (point-max))))))
		  
(defun org-current-is-next ()
  (string= "NEXT" (org-get-todo-state)))
#+END_SRC
Create timestamp when state changes from TODO or NEXT to DONE
#+BEGIN_SRC emacs-lisp
(setq org-log-done t)
#+END_SRC

** Keybindings

Global keybindings with =SPC= as prefix and emacs style with =C-c= as prefix.
#+BEGIN_SRC emacs-lisp
(general-def '(normal motion) 'override
  :prefix "SPC"
  "a" 'org-agenda
  "c" 'org-capture
  "l" 'org-store-link)
(general-def '(normal motion)
  "C-c l" 'org-store-link
  "C-c a" 'org-agenda
  "C-c c" 'org-capture)
#+END_SRC

Keybindings for navigation in org
#+BEGIN_SRC emacs-lisp
(general-def 'normal
  :keymaps 'org-mode-map
  "gj" 'org-next-visible-heading
  "gk" 'org-previous-visible-heading)
#+END_SRC

** Additional modules

Templates, e.g. for source code blocks

#+BEGIN_SRC emacs-lisp
(require 'org-tempo)
#+END_SRC

* Text completion framework

Company can use different backend to suggest completions. For example
from [[*Language Server setup][language servers]], buffers, TAGS and more.

#+BEGIN_SRC emacs-lisp
(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode))
#+END_SRC

* Programming
** Language Server setup
  
 Language server for an IDE-like Emacs setup. Among others, Python and
 Javascript are included and don't require additional packages.

 #+BEGIN_SRC emacs-lisp
 (use-package lsp-mode
   :commands lsp
   :hook
   '(javascript-mode
     python-mode
     ruby-mode))
 #+END_SRC

 Enable LSP as backend for company.

 #+BEGIN_SRC emacs-lisp
 (use-package company-lsp
   :init
   (push 'company-lsp company-backends))
 #+END_SRC

 Pop up diagnostics.

 #+BEGIN_SRC emacs-lisp
 (use-package flymake-diagnostic-at-point
   :after flymake
   :config
   (add-hook 'flymake-mode-hook #'flymake-diagnostic-at-point-mode))
 #+END_SRC
** Project Management
   
Projectile.el is a project management system that enables project-wide
file selection, grepping and more.

Ivy and counsel are used for completions.
#+BEGIN_SRC emacs-lisp
(use-package projectile
  :config
  (setq projectile-completion-system 'ivy)
  (projectile-mode 1))
(use-package counsel-projectile
  :config
  (counsel-projectile-mode 1))

#+END_SRC

*** Projectile keybindings

Emacs prefix style.
#+BEGIN_SRC emacs-lisp
(general-def
  "C-c p" 'projectile-command-map)
#+END_SRC

=SPC= prefix.
#+BEGIN_SRC emacs-lisp
(general-def '(normal motion) 'override
  :prefix "SPC"
  "p" 'projectile-command-map)
#+END_SRC
** Git
   
Magit.el brings Git into Emacs.
#+BEGIN_SRC emacs-lisp
(use-package magit
  :config
  (setq magit-completing-read-function 'ivy-completing-read))
#+END_SRC

Keybindings for magit.
#+BEGIN_SRC emacs-lisp
(use-package evil-magit)
(general-def '(normal motion) 'override
  :prefix "SPC"
  "g" 'magit-status
  "m" 'magit-file-dispatch)
#+END_SRC

** Programming and markup languages
*** Docker
#+BEGIN_SRC emacs-lisp
(use-package dockerfile-mode)
#+END_SRC
*** JSON
#+BEGIN_SRC emacs-lisp
(use-package json-mode)
(use-package json-reformat)
#+END_SRC
*** Markdown
#+BEGIN_SRC emacs-lisp
(use-package markdown-mode)
#+END_SRC
*** Salt
#+BEGIN_SRC emacs-lisp
(use-package salt-mode)
#+END_SRC
*** YAML
#+BEGIN_SRC emacs-lisp
(use-package yaml-mode)
#+END_SRC
*** Javascript
#+BEGIN_SRC emacs-lisp
(use-package typescript-mode)
#+END_SRC
*** PHP
#+BEGIN_SRC emacs-lisp
(use-package php-mode)
#+END_SRC
*** Python
#+BEGIN_SRC emacs-lisp
(setq python-shell-interpreter "python3")
(setq-default python-indent-offset 4)
#+END_SRC
*** Ruby on Rails
#+BEGIN_SRC emacs-lisp
(use-package projectile-rails
 :config
  (projectile-rails-global-mode))
#+END_SRC

Keybindings for projectile rails
#+BEGIN_SRC emacs-lisp
(general-def '(normal motion) 'override
  :prefix "SPC"
  "r m" 'projectile-rails-find-model
  "r M" 'projectile-rails-find-current-model
  "r c" 'projectile-rails-find-controller
  "r C" 'projectile-rails-find-current-controller
  "r v" 'projectile-rails-find-view
  "r V" 'projectile-rails-find-current-view
  "r h" 'projectile-rails-find-helper
  "r H" 'projectile-rails-find-current-helper
  "r l" 'projectile-rails-find-lib
  "r f" 'projectile-rails-find-feature
  "r p" 'projectile-rails-find-spec
  "r P" 'projectile-rails-find-current-spec
  "r t" 'projectile-rails-find-test
  "r T" 'projectile-rails-find-current-test
  "r n" 'projectile-rails-find-migration
  "r N" 'projectile-rails-find-current-migration
  "r u" 'projectile-rails-find-fixture
  "r U" 'projectile-rails-find-current-fixture
  "r j" 'projectile-rails-find-javascript
  "r s" 'projectile-rails-find-stylesheet
  "r o" 'projectile-rails-find-log
  "r i" 'projectile-rails-find-initializer
  "r e" 'projectile-rails-find-environment
  "r a" 'projectile-rails-find-locale
  "r @" 'projectile-rails-find-mailer
  "r !" 'projectile-rails-find-validator
  "r y" 'projectile-rails-find-layout
  "r k" 'projectile-rails-find-rake-task
  "r b" 'projectile-rails-find-job
  "r x" 'projectile-rails-extract-region
  "r . d" 'projectile-rails-dbconsole
  "r . c" 'projectile-rails-console
  "r . s" 'projectile-rails-server
  "r . r" 'projectile-rails-rake
  "r . g" 'projectile-rails-generate
  "r g f" 'projectile-rails-goto-file-at-point
  "r g g" 'projectile-rails-goto-gemfile
  "r g r" 'projectile-rails-goto-routes
  "r g d" 'projectile-rails-goto-schema
  "r g s" 'projectile-rails-goto-seeds
  "r g h" 'projectile-rails-goto-spec-helper)
#+END_SRC
*** Rust
#+BEGIN_SRC emacs-lisp
(use-package rust-mode)
(use-package cargo
  :hook 'rust-mode-hook)
(use-package flymake-rust)
#+END_SRC
*** Web development general
#+BEGIN_SRC emacs-lisp
(use-package web-mode
  :config
  (setq web-mode-markup-indent-offset 2))
#+END_SRC
* Email
  
** Initial setup

Email gets downloaded via ~mbsync~, ~w3m~ is used to display HTML mail
and ~mu~ is used for indexing and displaying the mail in Emacs (~mu4e~
is the name of the frontend).

Dependencies can be installed with ~zypper -n in isync w3m mu4e~.
A =.mbsyncrc= config file is needed, for obvious reasons it is not
contain within this repositories. If Nextcloud is setup, it can be
copied over from =~/Nextcloud=: ~cp ~/Nextcloud/configs/mbsync/mbsyncrc ~/.mbsyncrc~.

Before the first run, the root maildir folders need to be created:
~mkdir -p ~/Maildir/{suse,mailbox} && mbsync -a && mu index~


** mu4e configuration

*** Basic config

 Load installed mu4e.
 #+BEGIN_SRC emacs-lisp
 (add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")
 (require 'mu4e)
 #+END_SRC

 Set Maildir.
 #+BEGIN_SRC emacs-lisp
 (setq mu4e-maildir "~/Maildir")
 #+END_SRC

 Use ivy.
 #+BEGIN_SRC emacs-lisp
 (setq mu4e-completing-read-function 'ivy-completing-read)
 #+END_SRC

 mbsync requires filename changes when they are moved.
 #+BEGIN_SRC emacs-lisp
 (setq mu4e-change-filenames-when-moving t)
 #+END_SRC

 Automatic refresh via ~mbsync~ every 5 minutes.
 #+BEGIN_SRC emacs-lisp
 (setq mu4e-get-mail-command "mbsync -a")
 (setq mu4e-update-interval 300)
 (setq mu4e-index-cleanup nil)
 (setq mu4e-index-lazy-check t)
 #+END_SRC

*** Contexts

 Context are used to change directories and other settings for
 different email accounts or identities. The first is picked as default
 for reading (and refiling, deleting), in this case "work". Composing
 mail requires an explicit choice.

 Three contexts are defined here ("work", "mailbox", "politics")
 #+BEGIN_SRC emacs-lisp
 (setq mu4e-contexts
   `(  ,(make-mu4e-context
	:name "Work"
	:enter-func (lambda () (mu4e-message "Entering Work context"))
	:leave-func (lambda () (mu4e-message "Leaving Work context"))
	;; match based on Maildir
	:match-func (lambda (msg)
                      (when msg
			(string-match-p "^/suse/*" (mu4e-message-field msg :maildir))))
	:vars '( (user-mail-address      . "agraul@suse.com")
                 (user-full-name         . "Alexander Graul")
                 (smtpmail-smtp-server   . "imap.suse.de")
                 (smtpmail-smtp-user     . "agraul@suse.de")
                 (mu4e-sent-folder       . "/suse/Sent")
                 (mu4e-drafts-folder     . "/suse/Drafts")
                 (mu4e-trash-folder      . "/suse/Trash")
                 (mu4e-refile-folder     . "/suse/Archive")
                 (mu4e-compose-signature . t)))
       ,(make-mu4e-context
	:name "Mailbox"
	:enter-func (lambda () (mu4e-message "Entering Mailbox context"))
	:leave-func (lambda () (mu4e-message "Leaving Mailbox context"))
	;; match based on Maildir
	:match-func (lambda (msg)
                      (when msg
			(string-match-p "^/mailbox/*" (mu4e-message-field msg :maildir))))
	:vars '( (user-mail-address      . "mail@agraul.de")
                 (user-full-name         . "Alexander Graul")
                 (smtpmail-smtp-server   . "smtp.mailbox.org")
                 (smtpmail-smtp-user     . "alexander.graul@mailbox.org")
                 (mu4e-compose-signature . nil)
                 (mu4e-sent-folder       . "/mailbox/Sent")
                 (mu4e-drafts-folder     . "/mailbox/Drafts")
                 (mu4e-trash-folder      . "/mailbox/Trash")
                 (mu4e-refile-folder     . "/mailbox/Archive")))
      ,(make-mu4e-context
	:name "Politics"
	:enter-func (lambda () (mu4e-message "Entering Politics context"))
	:leave-func (lambda () (mu4e-message "Leaving Politics context"))
	:match-func (lambda (msg)
                      (when msg
			(mu4e-message-contact-field-matches msg
                           :to "politik@alexandergraul.de")))
	:vars '( (user-mail-address      . "politik@alexandergraul.de")
                 (user-full-name         . "Alexander Graul")
                 (smtpmail-smtp-user     . "alexander.graul@mailbox.org")
                 (smtpmail-smtp-server   . "smtp.mailbox.org")
                 (mu4e-sent-folder       . "/mailbox/Sent")
                 (mu4e-drafts-folder     . "/mailbox/Drafts")
                 (mu4e-trash-folder      . "/mailbox/Trash")
                 (mu4e-refile-folder     . "/mailbox/Archive")
                 (mu4e-compose-signature . nil)))))
 (setq mu4e-compose-context-policy 'ask)
 (setq mu4e-context-policy 'pick-first)
 #+END_SRC
**** TODO Automatic context switching does not work
*** Maildir shortcuts

#+BEGIN_SRC emacs-lisp
(setq mu4e-maildir-shortcuts
  '( ("/mailbox/INBOX"                  . ?m)
     ("/mailbox/INBOX/openSUSE/factory" . ?f)
     ("/suse/INBOX"                     . ?s)
     ("/suse/mailinglists-suse/kiga"    . ?k)
     ("/suse/mailinglists-suse/l3"      . ?l)))
#+END_SRC
*** Viewing mail

#+BEGIN_SRC emacs-lisp
(add-to-list 'mu4e-view-actions
  '("ViewInBrowser" . mu4e-action-view-in-browser) t)
(setq message-kill-buffer-on-exit t)
(setq mu4e-view-show-addresses 't)
(setq mu4e-headers-date-format "%e.%m.%y")
(setq mu4e-headers-fields '((:human-date . 10)
                            (:flags . 6)
                            (:mailing-list . 10)
                            (:from-or-to . 22)
                            (:subject)))
(setq mu4e-user-mail-address-list '("agraul@suse.com"
                                    "agraul@suse.de"
                                    "agraul@opensuse.org"
                                    "mail@agraul.de"
                                    "mail@alexandergraul.de"
                                    "politik@alexandergraul.de"))
#+END_SRC
*** Composing and sendingMail
#+BEGIN_SRC emacs-lisp
(setq mu4e-compose-format-flowed t)
(setq message-signature-file "~/Nextcloud/SUSE/signature.txt")
(setq mail-user-agent 'mu4e-user-agent)
(setq message-send-mail-function 'smtpmail-send-it)
#+END_SRC
*** org mode integration

Store link to message if in header view, not to header query
#+BEGIN_SRC emacs-lisp
(setq org-mu4e-link-query-in-headers-mode nil)
#+END_SRC
* UI
** Frame

No bars.
#+BEGIN_SRC emacs-lisp
(if (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode)
  (menu-bar-mode -1))
#+END_SRC

Inlcude buffer name in window title
#+BEGIN_SRC emacs-lisp
(setq-default frame-title-format "%b - emacs")
#+END_SRC

** Themes and colors

Different colors for matching parenthesis
#+BEGIN_SRC emacs-lisp
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))
#+END_SRC

Themes
#+BEGIN_SRC emacs-lisp
(use-package gruvbox-theme)
(use-package doom-themes
  :config
  (load-theme 'doom-one t)
  (doom-themes-org-config))
#+END_SRC

Modeline, requires icon fonts to be installed (~M-x all-the-icons-install-fonts~)
#+BEGIN_SRC emacs-lisp
(use-package doom-modeline
  :hook (after-init . doom-modeline-mode))
(column-number-mode 1)
#+END_SRC

** Font

#+BEGIN_SRC emacs-lisp
(add-to-list 'default-frame-alist
             '(font . "Input Mono Condensed 12"))
#+END_SRC

Enable font ligatures (Fira Code)
#+begin_src emacs-lisp
(defun fira-code-mode--make-alist (list)
  "Generate prettify-symbols alist from LIST."
  (let ((idx -1))
    (mapcar
     (lambda (s)
       (setq idx (1+ idx))
       (let* ((code (+ #Xe100 idx))
          (width (string-width s))
          (prefix ())
          (suffix '(?\s (Br . Br)))
          (n 1))
     (while (< n width)
       (setq prefix (append prefix '(?\s (Br . Bl))))
       (setq n (1+ n)))
     (cons s (append prefix suffix (list (decode-char 'ucs code))))))
     list)))

(defconst fira-code-mode--ligatures
  '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\"
    "{-" "[]" "::" ":::" ":=" "!!" "!=" "!==" "-}"
    "--" "---" "-->" "->" "->>" "-<" "-<<" "-~"
    "#{" "#[" "##" "###" "####" "#(" "#?" "#_" "#_("
    ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*"
    "/**" "/=" "/==" "/>" "//" "///" "&&" "||" "||="
    "|=" "|>" "^=" "$>" "++" "+++" "+>" "=:=" "=="
    "===" "==>" "=>" "=>>" "<=" "=<<" "=/=" ">-" ">="
    ">=>" ">>" ">>-" ">>=" ">>>" "<*" "<*>" "<|" "<|>"
    "<$" "<$>" "<!--" "<-" "<--" "<->" "<+" "<+>" "<="
    "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<" "<~"
    "<~~" "</" "</>" "~@" "~-" "~=" "~>" "~~" "~~>" "%%"
    "x" ":" "+" "+" "*"))

(defvar fira-code-mode--old-prettify-alist)

(defun fira-code-mode--enable ()
  "Enable Fira Code ligatures in current buffer."
  (setq-local fira-code-mode--old-prettify-alist prettify-symbols-alist)
  (setq-local prettify-symbols-alist (append (fira-code-mode--make-alist fira-code-mode--ligatures) fira-code-mode--old-prettify-alist))
  (prettify-symbols-mode t))

(defun fira-code-mode--disable ()
  "Disable Fira Code ligatures in current buffer."
  (setq-local prettify-symbols-alist fira-code-mode--old-prettify-alist)
  (prettify-symbols-mode -1))

(define-minor-mode fira-code-mode
  "Fira Code ligatures minor mode"
  :lighter " Fira Code"
  (setq-local prettify-symbols-unprettify-at-point 'right-edge)
  (if fira-code-mode
      (fira-code-mode--enable)
    (fira-code-mode--disable)))

(defun fira-code-mode--setup ()
  "Setup Fira Code Symbols"
  (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol"))

(provide 'fira-code-mode)
#+end_src


** Directory listing

Neotree adds a directoy tree buffer that can be shown / hidden on a keypress.
[[*Evil keybindings for other modes][evil-collection]] adds Vim keybindings for it.
#+begin_src emacs-lisp
(use-package neotree
  :config
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow)))
(general-def '(normal motion) 'override
  :prefix "SPC"
  "q" 'neotree-toggle)
#+end_src

* General Behaviour

No bell.
#+BEGIN_SRC emacs-lisp
(setq ring-bell-function 'ignore)
#+END_SRC

No startup screen
#+BEGIN_SRC emacs-lisp
(setq inhibit-startup-screen t)
#+END_SRC

y/n prompts for everything.
#+BEGIN_SRC emacs-lisp
(defalias 'yes-or-no-p 'y-or-n-p)
#+END_SRC


* Backups and Autosaves
  
Backups are copies that are created when a file is edited for the
first time. Since Git is used for version control, this is not wanted.

#+BEGIN_SRC emacs-lisp
(setq make-backup-files nil)
#+END_SRC

Autosaves are periodical saves. These are fine, but should be in a
single directory =~/emacs.d/backups=. Directory must exist.
#+BEGIN_SRC sh
mkdir ~/.emacs.d/backups
#+END_SRC

#+BEGIN_SRC emacs-lisp
(setq auto-save-file-name-transforms
      `((".*" ,(concat user-emacs-directory "backups/") t)))
#+END_SRC

