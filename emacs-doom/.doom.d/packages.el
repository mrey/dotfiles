;; -*- no-byte-compile: t; -*-
;;; ~/.doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:fetcher github :repo "username/repo"))
;; (package! builtin-package :disable t)
;; (package! mu4e-alert)
(package! dockerfile-mode)
(package! salt-mode)
(package! tao-theme)
