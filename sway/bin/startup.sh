#!/bin/bash
# Background services
swaymsg exec "gnome-keyring-daemon --daemonize"
swaymsg exec /usr/lib/polkit-gnome-authentication-agent-1
swaymsg exec "nm-applet --indicator"
swaymsg exec "nextcloud --background"
swaymsg exec mako

# Workspaces setup
swaymsg workspace 2 && swaymsg exec emacs-x11
swaymsg workspace 3 && swaymsg exec firefox
swaymsg workspace 4 && swaymsg exec hexchat
swaymsg workspace 8 && swaymsg exec thunderbird
swaymsg workspace 9 && swaymsg exec flatpak run com.spotify.Client
swaymsg workspace 10 && swaymsg exec keepassxc

# End up at 1
swaymsg workspace 1
