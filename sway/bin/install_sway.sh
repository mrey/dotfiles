#!/usr/bin/bash

if [[ $UID == 0 ]];
then
    ZYPPER="zypper";
else
    ZYPPER="sudo zypper";
fi

$ZYPPER install sway \
    swayidle swayidle-bash-completion swayidle-zsh-completion \
    swaylock swaylock-bash-completion swaylock-zsh-completion \
    waybar \
    rofi \
    pavucontrol \
    fontawesome-fonts \
    brightnessctl
