# start sway automatically from tty1
if [ "$(tty)" = "/dev/tty1" ]; then
    export XDG_SESSION_DESKTOP="sway"
    exec sway
fi

# Prompt https://github.com/sindresorhus/pure 
# fpath is adjusted to include prompt_pure_setup and async
fpath=($fpath ~/.local/share/zsh/site-functions)
autoload -U promptinit; promptinit
PURE_PROMPT_SYMBOL="↳"
prompt pure

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f ~/.aliases ] && source ~/.aliases

# silence find
ffind() {
    find "$@" 2>/dev/null
}
# launch disowned
detachlaunch() {
   nohup "$@" & disown
}

# The following lines were added by compinstall
zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' completions 1
zstyle ':completion:*' file-sort name
zstyle ':completion:*' glob 1
zstyle ':completion:*' insert-unambiguous false
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 1 numeric
zstyle ':completion:*' original false
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/alex/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source ~/.local/share/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.local/share/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
