# Environmental stuff
export TERM="xterm-256color"
export LANG=en_US.UTF-8
export SSH_KEY_PATH="~/.ssh/id_rsa"
export PAGER='less'
export MAN_POSIXLY_CORRECT=true # nicer manpage handling
#export MOZ_ENABLE_WAYLAND=1
export EDITOR='emacsclient -n'
export WORDCHARS='*?_-.[]~=&;!#$&(){}<>' # default without '/

[ "$XDG_SESSION_DESKTOP" = "sway" ] && export QT_QPA_PLATFORMTHEME="wayland"
[ "$QT_QPA_PLATFORMTHEME" = "wayland" ] && export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"

# FZF
# --files: List files that would be searched but do not search
# --no-ignore: Do not respect .gitignore, etc...
# --hidden: Search hidden files and folders
# --follow: Follow symlinks
# --glob: Additional conditions for search (in this case ignore everything in the .git/ folder)
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'

# rbenv manages multiple ruby versions
# and is cloned into ~/.rbenv
# https://github.com/rbenv/rbenv
which rbenv >/dev/null 2>&1 && eval "$(rbenv init -)"

# Unlock SSH key at session start for "custom DEs"
SSH_ENV="$HOME/.ssh/environment"
function start_agent {
        echo "Initialising new SSH agent..."
        /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
        echo succeeded
        chmod 600 "${SSH_ENV}"
        . "${SSH_ENV}" > /dev/null
        /usr/bin/ssh-add -t 432000 ;
}

if [ "${XDG_SESSION_DESKTOP}" = "sway" ]; then
    if [ -f "${SSH_ENV}" ]; then
        . "${SSH_ENV}" > /dev/null
        ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
            start_agent;
        }
    else
        start_agent;
    fi
fi
