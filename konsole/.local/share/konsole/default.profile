[Appearance]
ColorScheme=gruvbox_dark

[Cursor Options]
CursorShape=0

[General]
Name=default
Parent=/home/alex/.local/share/konsole/gruvbox.profile

[Terminal Features]
BlinkingCursorEnabled=0
