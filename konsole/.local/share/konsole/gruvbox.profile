[Appearance]
AntiAliasFonts=true
BoldIntense=false
ColorScheme=Gruvbox Hard
Font=IBM Plex Mono,12,-1,5,57,0,0,0,0,0,Medium
UseFontLineChararacters=false

[Encoding Options]
DefaultEncoding=UTF-8

[General]
Command=/usr/bin/zsh
Name=gruvbox
Parent=FALLBACK/
StartInCurrentSessionDir=true

[Interaction Options]
OpenLinksByDirectClickEnabled=true

[Scrolling]
HistoryMode=2
ScrollBarPosition=2
