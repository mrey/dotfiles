[Appearance]
BoldIntense=true
ColorScheme=Light

[Cursor Options]
CursorShape=0

[General]
Name=light
Parent=/home/alex/.local/share/konsole/gruvbox.profile

[Terminal Features]
BlinkingCursorEnabled=false
